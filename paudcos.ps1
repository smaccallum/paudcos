# paudcos.ps1
# Podcast audio conversion software
# Copyright (C) 2021  Scott C. MacCallum

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.

# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# Fetch a m4a file from the URL that the user provides
# Option -Prompt, prompt the user
# Option -AsSecureString, receive the input from the user as a secure string
# Option -Uri, sets the uniform resource identifier

Write-Output "`nDownloading the m4a file...`n"

$url= Read-Host -Prompt "Online location of m4a file (link)"

$name= Read-Host -Prompt "`nSave as file name (without the .m4a file extension)"

Invoke-WebRequest -Uri "$url" -outfile "$name"

# Rename the file that the user saved to add the .m4a extension. A clever way to
# make the file available to a conversion program and have a file name variable
# wihout a file extension that we can change as needed for a conversion program.

Rename-Item "$name" -NewName "$name.m4a"

Write-Output "`nConverting the m4a file to raw format..."

$name_raw= "$name.raw"

# Convert a m4a file to a mono 44.1 kHz 64kbps 16-bit raw file
# Option -i, input
# Option -ac 1, sets the audio to mono
# Option -f s16le, forces input of the file format to PCM signed 16-bit little-endian

.\ffmpeg -i "$name.m4a" -ac 1 -f s16le "$name_raw"

Write-Output "`nConverting the raw file to mp3 format..."

$name_mp3= "$name.mp3"

# Convert a raw file to a mono 44.1 kHz 64kbps 16-bit mp3 file
# Option --verbose, prints extra information during file conversion
# Option -m m, sets the audio to mono
# Option -s, sets the kHz
# Option -b, sets the kbps
# Option --cbr, sets a constant kbps
# Option --bitwidth, sets the number of bits of the resulting file

.\lame --verbose -m m -s 44.1 -b 64 --cbr --bitwidth 16 "$name_raw" "$name_mp3"